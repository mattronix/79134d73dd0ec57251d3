### Keybase proof

I hereby claim:

  * I am mattronix on github.
  * I am mattronix (https://keybase.io/mattronix) on keybase.
  * I have a public key whose fingerprint is 3E6F 9570 9C6C 6F0F DC4F  C5B2 1F5C 8E8A 1881 68FD

To claim this, I am signing this object:

```json
{
    "body": {
        "key": {
            "fingerprint": "3e6f95709c6c6f0fdc4fc5b21f5c8e8a188168fd",
            "host": "keybase.io",
            "key_id": "1f5c8e8a188168fd",
            "uid": "dc51eda9132dd566f4c8ea2180016a00",
            "username": "mattronix"
        },
        "service": {
            "name": "github",
            "username": "mattronix"
        },
        "type": "web_service_binding",
        "version": 1
    },
    "ctime": 1407268570,
    "expire_in": 157680000,
    "prev": "0c0693fbb6cc4b25cfa579da64582b09385ea352ab165c6b40e22d5599e666ce",
    "seqno": 3,
    "tag": "signature"
}
```

with the PGP key whose fingerprint is
[3E6F 9570 9C6C 6F0F DC4F  C5B2 1F5C 8E8A 1881 68FD](https://keybase.io/mattronix)
(captured above as `body.key.fingerprint`), yielding the PGP signature:

```
-----BEGIN PGP MESSAGE-----
Version: GnuPG v2

owGbwMvMwMQoH9PXJdGY8Zfx9IG3SQzBD80eVCsl5adUKllVK2Wngqm0zLz01KKC
osy8EiUrJeNUszRLU3MDy2SzZLM0g7SUZJO0ZNMkI8M002SLVItEQwsLQzOLtBQl
HaWM/GKQDqAxSYnFqXqZ+UAxICc+MwUoikV9KVgiJdnUMDUl0dLQ2CglxdTMLM0E
qC7RyNDCwMDQLNHAAKSwOLUoLzE3Fag6N7GkpCg/L7NCqVZHCShclpmcCnI0VDo9
sySjNAmPlpLKApBYeWpSPFR3fFJmXgrQy0BNZalFxZn5eUpWhkCVySWZIO2GJgbm
RmYWwBDQUUqtKMgsSo3PBKkwNTcDutAAKFpQlFoGNNIg2cDM0jgtKcksOdkkycg0
OS3R1NwyJdHMxNTCKMnA0tjCNDXR2NQoMcnQzDTZLMnEINXIKMXU1NIy1czMDOgL
kH8K8/KVrIyBzkxMBxpZnJmel1hSWpSqVMvVySTDwsDIxMDGygSKNQYuTgFYXK6x
EWBYcmjD/7NWujZb5jGtCtt9ottgzypFC2vJnVesv7G+cUo3853791n4qWdxn2Vu
bIhRmNfyS7dwaccL9Y+fs4I8onf1uzdeKjCbqpH3s18tp33v0y3ce/R53+REa3uz
yk1lvMMUbS/4/8/ry1sV1PMjmu9NV5c69/1zkkbv1qVHSpP+q7R+r+wpq/h4aGmq
78HiLXZTpyw7LHHj1YV7U0z0GJhucRfuKPFc+HNS2N6vL1b/Tn3gUdS1189F55Yu
Q52qV65Tu3WeoETz5W1v/bn5XymmdLYdbKq/xH1DKZd10y7714FNm2WCJomoe8ld
/f1CRJ2Bx/m7JevMeZcdL/wx/DqttUzmn/vWQ3fvJrl/VK4S37RXhjH0XHQL57Wf
Zf9KWBZc5OV0fS4txR1c/Lzh/gkRNVn/0NtbZum/at6xpjNqg5uQzvVDmic+C/vM
KXo28WnFw9eTdP2Kmv4dcugoDZjkJXy7Y/OTE3mtiqenN15fFtf4XeVgxG4ZSdXf
L5aqZnz74rW4e/uNN7Mk37Wn3vibt1Xp1O9IyQuHvuf3rnX4vf7LdOH1lhkSM6Qa
QkVNsxmygv5e8v5+esuhEy4pSqtL9Xr+3NCceG3DvlJ21xz/VpPkXxMOpjNKq8yr
rNjtZnroXFaJuOn0jA8bbYMmnpmhfsdp48cAhTvJ24R/J7uUV3TMrl+7gen8ZINV
qxb/tqzzmHHdP7341LRcAA==
=WN3a
-----END PGP MESSAGE-----

```

And finally, I am proving ownership of the github account by posting this as a gist.

### My publicly-auditable identity:

https://keybase.io/mattronix

### From the command line:

Consider the [keybase command line program](https://keybase.io/docs/command_line).

```bash
# look me up
keybase id mattronix

# encrypt a message to me
keybase encrypt mattronix -m 'a secret message...'

# ...and more...
```
